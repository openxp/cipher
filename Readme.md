
![Scheme](application.png)

# Cipher XP library

---
**Important build info**
Existing builds with dependency from maven url https://dl.bintray.com/openxp/public will break as bintray services are deprecated on May 1st 2021. Fix this by replacing maven url with following https://openxp.jfrog.io/artifactory/public.

---

A library for Enonic XP that allows encrypting and decrypting with strong symmetric password based encryption.

## Why creating this library

GDPR regulation does require you to enforce security measures and safeguards to protect any piece of information that relates to an identifiable person. This library is a first step in implementing a solution that could be used for this
purpose.

## How secure is it

As encryption is based on a common shared key generated in Chipher.java, this method of encrypting your data is only as 
secure as your password. It is recommended to use strong passwords with 15+ characters containing mixed-case letters, 
numbers and special characters. Someone with access to your data could brute-force your password - and an ill-intentioned 
application deployed in the same enviroment as your app could get hold of your password from system properties or 
configuration, and decrypt the data using this password with the code available in this library. So, use a strong
password, and decide for yourself if this is secure "enough" for you application. This application has not been through
any security audit, but the sourcecode is open source and therefore possible to review.

## How can security be improved in the future?

An idea is to implementing asymmetric encryption using public/private key pair. This would introduce setup complexity. 
One would need to make keys available across (possible) multiple Enonic XP nodes in a cluster. This would probably mean
that a system administrator would have to manually install key in multiple keystores or one would have to use a 
centralized keystore, such as AWS Key Management Service (KMS), CyberArk, Hashicorp Vault, or Azure Key Vault, to better 
secure credentials.

## Example on when using this library could be useful

If you have a production system that persist sensitive customer data into the Enonic XP storage, and that data is exported
to a QA, testing or development environment, and you want this data to be encrypted there. 

# Usage

Add repository to the repository list in the build.gradle file:

```groovy
    repositories {
        maven {
            url 'https://openxp.jfrog.io/artifactory/public'
        }
    }
```

After this, add the following dependency:

```groovy
    dependencies {
        include 'openxp.app:cipher:1.0.0'
    }
```

# Encrypting and decrypting data

Use the following methods for encrypting and decrypting data

```javascript
var secret = cipherLib.getSecret();
cipherLib.encrypt(data, secret)
cipherLib.decrypt(data, secret)    
```

`data` should be a string value and `secret` is the encryption password.


## Configuring encryption password

Password can be either be set

1. on the server / service by adding the environmental property 'openxp.lib.cipher.secret'
2. in the .cfg file specific for you app. See [documentation](https://developer.enonic.com/docs/xp/stable/deployment/config#application_config) for details.

### Example on how to start server with environment property

`./bin/server.sh debug -Dopenxp.lib.cipher.secret="fisk"`


### Example on how to define property on $xp.home/config/[app.name].cfg

`openxp.lib.cipher.secret=fisk er godt til middag`


Password can can be retrieved in your app by calling cipherLib.getSecret(). This method will look
for environmental property first and app config second.


## Example usage on the command line


Start the jar file without any commands will trigger console session:

```
java -jar build/libs/cipher-1.0.1.jar

Usage:
 cipher-[version].jar [encrypt | decrypt] [password] "[text to encrypt / decrypt]"

Do you want to encrypt or decrypt?
encrypt
Password:

Enter plaintext to encrypt
My text to encrypt
AAAADHSwzmtvNCu3DT6PUCiExu1pSMjDpAPneTkM7YtSha6JaTYdZeMGLnlyEjbhZHE=
```

## Other command line examples

```bash
java -jar cipher-1.0.1.jar encrypt mySecret "My secret text to encrypt"
java -jar cipher-1.0.1.jar decrypt mySecret AAAADFODgpHMt1dAmWlDzJfFFOs53Q8s18c+s6kEuW+Yj4oAG5v01a1pFiWm+uTgxE+JmAS7aTJS
java -jar cipher-1.0.1.jar encrypt mySecret "$(< myPlaintextInAFile.txt)"
java -jar cipher-1.0.1.jar encrypt mySecret "$(< myPlaintextInAFile.txt)" >myEncryptedTextInAFile.txt
java -jar cipher-1.0.1.jar decrypt mySecret "$(< myEncryptedTextInAFile.txt)" >myDecryptedText.txt
```
