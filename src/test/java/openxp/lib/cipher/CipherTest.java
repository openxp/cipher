package openxp.lib.cipher;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.rmi.UnexpectedException;
import java.util.stream.Stream;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CipherTest {
    private Cipher cipher;

    @BeforeEach
    public void init(){
        cipher = new Cipher();
    }

    private static Stream<Arguments> secretPassword_plaintext() {
        return Stream.of(
                Arguments.of("secret", "Plain text to cipher"),
                Arguments.of("secret word", "Plain text to cipher"),
                Arguments.of("23r å2jf2å3joæLMW3", "23fæ23få23<23%&(/()&%$#fjpæ23å+r923 f23få232f3jopå"),
                Arguments.of("secret", ""),
                Arguments.of("secret", ""),
                Arguments.of("secret", "{\n\"email\": \"me@me.no\"\n}")

        );
    }

    @ParameterizedTest(name = "{index} => secret={0}, plaintext={1}")
    @MethodSource("secretPassword_plaintext")
    void GivenCorrectPassword_WhenEncryptDecrypt_PlaintextResultShouldPresentAndEqual(String password, String plaintext)
    throws UnexpectedException, IllegalArgumentException {
        String encryptedText = cipher.encrypt(plaintext, password);
        String decryptedText = cipher.decrypt(encryptedText, password);
        Assertions.assertEquals(plaintext, decryptedText);
    }

    private static Stream<Arguments> correctPassword__wrongPassword_plaintext() {
        return Stream.of(
                Arguments.of("secret", "notSecret", "Unequal passwords"),
                Arguments.of("secret word", "secretword", "Same letters with and without space"),
                Arguments.of("secret", "SECRET", "Password with non-equal case"),
                Arguments.of(" ", "  ", "Blank passwords with non-equal length")
        );
    }

    @ParameterizedTest(name = "{index} = correctPassword{0}, wrongPassword{1}, plaintext{2}")
    @MethodSource("correctPassword__wrongPassword_plaintext")
    void GivenWrongPassword_WhenEncryptDecrypt_ShouldThrowUnexpectedException(String password, String wrongPassword, String plaintext)
    throws UnexpectedException, IllegalArgumentException{
        String encryptedText = cipher.encrypt(plaintext, password);
        Assertions.assertThrows(UnexpectedException.class, () -> cipher.decrypt(encryptedText, wrongPassword));

    }

    private static Stream<Arguments> password_specialvalue() {
        return Stream.of(
                Arguments.of("secret", ""),
                Arguments.of("secret", "-1"),
                Arguments.of("secret", "0"),
                Arguments.of("secret", "null"),
                Arguments.of("  ", ""),
                Arguments.of("secret", "undefined")
        );
    }

    @ParameterizedTest(name = "{index} = password{0}, specialValue{1}")
    @MethodSource("password_specialvalue")
    void GivenIdenticalValue_WhenEncryptDecryptTwice_SaltShouldEnsureUniqueResult(String password, String specialvalue)
    throws IllegalArgumentException, UnexpectedException{
        Assertions.assertNotEquals(cipher.encrypt(specialvalue, password), cipher.encrypt(specialvalue, password));
    }

    private static Stream<Arguments> password_plaintext_valuesThatShouldTriggerExceptions() {
        return Stream.of(
                Arguments.of(null, ""),
                Arguments.of("", ""),
                Arguments.of("secret", null),
                Arguments.of(null, null)
        );
    }

    @ParameterizedTest(name = "{index} = password{0}, plainText{1}")
    @MethodSource("password_plaintext_valuesThatShouldTriggerExceptions")
    void GivenNullAsInput_WhenEncrypt_ShouldThrowIllegalArgumentException(String password, String plaintext){
        Assertions.assertThrows(IllegalArgumentException.class, () -> cipher.encrypt(plaintext, password));
    }

}
