var cipherBean = __.newBean('openxp.lib.cipher.Cipher');

var encrypt = function(plainText, secret){
    return cipherBean.encrypt(plainText, secret);
}

var decrypt = function(encyptedText, secret){
    return cipherBean.decrypt(encyptedText, secret);
}

var getSecret = function(){
    var secret = cipherBean.getSecret();
    if (!secret){
        secret = app.config['openxp.lib.cipher.secret'];
    }
    if (!secret) {
        log.warning("openxp.lib.cipher.secret could not be found in environment or app config!");
    }
    return secret;
}

exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.getSecret = getSecret;