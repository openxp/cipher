package openxp.lib.cipher;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.rmi.UnexpectedException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Cipher {
    private static final Logger LOGGER = Logger.getLogger(Cipher.class.getName());
    private static final int PBKDF2_ITERATIONS = 1000;
    private static final int AUTH_TAG_LENGHT = 128;

    public static SecretKey generateSecretKey(String password, byte[] iv) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), iv, PBKDF2_ITERATIONS, AUTH_TAG_LENGHT);
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte[] key = secretKeyFactory.generateSecret(spec).getEncoded();
        return new SecretKeySpec(key, "AES");
    }

    public String encrypt(String plaintext, String password) throws IllegalArgumentException, UnexpectedException{


        if (Objects.isNull(plaintext)) throw new IllegalArgumentException("plaintext to be encrypted cannot be null");
        if (Objects.isNull(password)) throw new IllegalArgumentException("encryption password cannot be null");
        if (password.isEmpty()) throw new IllegalArgumentException("encryption password cannot be empty");

        try {

            // GENERATE random nonce (number used once)
            SecureRandom random = SecureRandom.getInstanceStrong();
            final byte[] iv = new byte[12];
            random.nextBytes(iv);

            //Prepare your key/password
            SecretKey secretKey = generateSecretKey(password, iv);

            // ENCRYPTION
            javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec spec = new GCMParameterSpec(AUTH_TAG_LENGHT, iv);
            cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, secretKey, spec);

            byte[] encryptedData = cipher.doFinal(plaintext.getBytes(StandardCharsets.ISO_8859_1));

            //Concatenate everything and return the final data
            ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + encryptedData.length);
            byteBuffer.putInt(iv.length);
            byteBuffer.put(iv);
            byteBuffer.put(encryptedData);

            //return new String(byteBuffer.array(), StandardCharsets.ISO_8859_1);
            return Base64.getEncoder().encodeToString(byteBuffer.array());

        } catch (InvalidKeySpecException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidParameterException e) {
            LOGGER.log(Level.SEVERE, e.getLocalizedMessage());
            throw new UnexpectedException(e.getLocalizedMessage());
        }
    }

    public String decrypt(String encryptedText, String password) throws IllegalArgumentException, UnexpectedException{

        if (Objects.isNull(encryptedText)) throw new IllegalArgumentException("encryptedByteArray to be decrypted cannot be null");
        if (Objects.isNull(password)) throw new IllegalArgumentException("decryption password cannot be null");
        if (password.isEmpty()) throw new IllegalArgumentException("decryption password cannot be empty");

        try {
            //Wrap the data into a byte buffer to ease the reading process
            //ByteBuffer byteBuffer = ByteBuffer.wrap(encryptedText.getBytes(StandardCharsets.ISO_8859_1));
            ByteBuffer byteBuffer = ByteBuffer.wrap(Base64.getDecoder().decode(encryptedText));
            int noonceSize = byteBuffer.getInt();

            //Make sure that the file was encrypted properly
            if (noonceSize < 12 || noonceSize >= 16) {
                throw new IllegalArgumentException("Nonce size is incorrect. Make sure that the incoming data is an AES encrypted file.");
            }
            byte[] iv = new byte[noonceSize];
            byteBuffer.get(iv);

            //Prepare your key/password
            SecretKey secretKey = generateSecretKey(password, iv);

            //get the rest of encrypted data
            byte[] cipherBytes = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherBytes);

            javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(AUTH_TAG_LENGHT, iv);

            //Encryption mode on!
            cipher.init(javax.crypto.Cipher.DECRYPT_MODE, secretKey, parameterSpec);

            //Cipher the data
            byte[] decryptedBytes = cipher.doFinal(cipherBytes);
            String decryptedString = new String(decryptedBytes, StandardCharsets.ISO_8859_1);
            return decryptedString;

        } catch (InvalidKeySpecException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidParameterException | InvalidAlgorithmParameterException e) {
            LOGGER.log(Level.SEVERE, e.getLocalizedMessage());
            throw new UnexpectedException(e.getLocalizedMessage());
        }

    }

    public String getSecret(){
        return System.getProperty("openxp.lib.cipher.secret");
    }
}