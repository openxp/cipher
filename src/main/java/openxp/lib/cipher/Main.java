package openxp.lib.cipher;

import java.io.Console;
import java.io.PrintWriter;
import java.rmi.UnexpectedException;

public class Main {

    public static void main(String args[]) throws UnexpectedException {
        String command = "";
        String password;
        String plaintext;

        if (args == null || args.length < 3) {
            Console console = System.console();
            console.printf("Usage:%n cipher-[version].jar [encrypt | decrypt] [password] \"[text to encrypt / decrypt]\"%n%n");
            while (!("encrypt".equals(command) || "decrypt".equals(command))) {
                console.printf("Do you want to encrypt or decrypt?%n");
                command = console.readLine();
            }

            console.printf("Password:%n");
            char[] passwordArray = console.readPassword();
            password = new String(passwordArray);
            console.printf("Enter plaintext to " + command + "%n");
            plaintext = console.readLine();

            if (command == null || "".equals(command) ||
                    !("encrypt".equals(command) || "decrypt".equals(command)) ||
                    password == null || "".equals(password) ||
                    plaintext == null || "".equals(plaintext)) {
                System.out.println("Usage 'Cipher [encrypt|decrypt] [password] [plaintext]");
                return;
            }
        }else{
            command = args[0];
            password = args[1];
            plaintext = args[2];
        }

        Cipher cipher = new Cipher();
        String result = "";

        if ("encrypt".equals(command)) {
            result = cipher.encrypt(plaintext, password);
        } else if ("decrypt".equals(command)) {
            result = cipher.decrypt(plaintext, password);
        }
        PrintWriter out = new PrintWriter(System.out);
        out.write(result);
        out.flush();
        out.close();
    }
}
